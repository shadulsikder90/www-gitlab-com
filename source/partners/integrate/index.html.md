---
layout: markdown_page
title: "Partner with GitLab"
description: Learn about integrating with GitLab, as well as partnership, marketing, and licensing opportunities.
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Collaborate and Integrate with GitLab

GitLab is open to collaboration and committed to building technology partnerships in the DevOps ecosystem to enable customer success. We are open to adding new integrations to our [technology partners page](https://about.gitlab.com/partners) in a self-service way.

We support integrations through our APIs or [direct additions to our product](https://about.gitlab.com/handbook/product/#avoid-plugins-and-marketplaces), and we encourage integration partners to make and maintain those integration with us. The work required to deliver the integration will be provided by the partner. 

If you want to talk to us about a partnership you can contact the [Alliances team](mailto:Alliance@gitlab.com) or [open a new issue](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_partner).

## Requirements to become an Ecosystem partner 
The requirements to be listed on our Technology Partners page include: 
* Completed integration via [API](https://docs.gitlab.com/ee/api/), [webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) or [CI templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates). 
* Must have public documentation on the integration showcasing how users can get started
* One identified shared customer or specific use case that the integration helps solve

_See current examples on our [partner listing page](https://about.gitlab.com/partners)._

>Note: As we further develop our engagement with ecosystem partners, we also reserve the right to promote specific best practices to integrate with GitLab across our product's DevOps stages. To keep consistency and retain value to the UX within GitLab with external integration, GitLab may document details that give guidance on how specific types of integrations are expected to interoperate with GitLab. 

Companies that are interested in becoming an ecosystem partner, and have the above requirements ready, they should follow the steps below for getting listed on our Partners page. 

## Steps to getting listed on our Partner page 

#### Step 1: Submit a new partner issue 
If you are interested in partnering with GitLab and have completed an integration with us, please start by submitting a [New Partner issue](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_partner). When creating an issue, please select *new_partner* issue template in the drop down. 

The Alliances team manages new partner requests and will review the issue. Generally, you will receive a response from GitLab within two weeks of submitting a new partner issue. This will either be an update on the status or a request for additional information. If you have any questions about the status of your issue, please reach out to Alliance@gitlab.com for support.
 
#### Step 2: Adding your app to the Partners page
 
The next step is to get your app listed. Partners with technology integrations are asked to submit a Merge Request to add themselves to the [GitLab Technology Partners page](https://about.gitlab.com/partners/). Follow these instructions on [how to get your app listed](/handbook/alliances/integration-instructions/).

#### Step 3: The Alliance team will review your information  

Once the Merge Request has been created, the Alliances team will be notified and will review the information. If the requirements are met and ready for approval, the Alliances team will approve the MR to get your app listed on our website. 

 
#### Step 4: Self-service GTM opportunities

We love spreading the word on new integrations with GitLab to our community and followers. We help promote ecosystem partners that have an integration or feature with us.

>Note: This does require identifying a mutual customer or proof point ahead of time. 

### Brand Awareness 

- **Partner Listing**: You will be listed on [GitLab’s Partner Page](https://about.gitlab.com/partners).
- **Co-Sponsorship Opportunities**: If you’re interested in sponsoring GitLab related events, like [GitLab Commit User Conference](https://about.gitlab.com/events/commit/#sponsor) or any other marketing moments, reach out to our Partner Marketing team.
- **Community Engagement**: We encourage our partners to participate in the GitLab community, for example: [contributing](/community/contribute/) to GitLab FOSS, speaking at [GitLab Meetups](/community/meetups/), participating in [GitLab Heroes](/community/heroes/), or engaging the community in other ways. Partners are welcome to bring questions or ideas around growing our communities directly to our Community Relations team via [evangelists@gitlab.com](mailto:evangelists@gitlab.com).  

## Marketing and External Communications
- **Public Relations (PR)/ External Communications Requests**: If you choose to make a formal announcement around your partnership with GitLab through a press release or blog post, you can submit a request for a supporting quote from GitLab through the new partner issue template. In the partner issue template, you will be able to select the PR checkbox and provide additional information in the comments section. For review and approval, we will need approximately 5-7 business days to review the content and secure approvals from the appropriate executives and internal SMEs. Additional information needed for the request:
  * Give a brief summary about the partnership you are wanting to announce. Is your integration with GitLab tied to an upcoming release?
  * Are you aiming to announce from a specific event or conference?
  * Is there a joint customer case study to reference?
  * Who is the GitLab product manager and product marketing manager that you are working with on the integration/partnership?
  * Are you planning to do media outreach around your announcement? If so, are you wanting to have a GitLab spokesperson available?
- **Guest Blogging on the Joint Solution**: We request the blog and content include a customer reference or equivalent proof point of the joint solution that supports the use case being discussed. Follow these [guidelines](https://about.gitlab.com/handbook/marketing/blog/#guest-posts-by-gitlab-partners) when submitting guest posts to [about.gitlab.com/blog](https://about.gitlab.com/blog).  
> Please note that a blog post may take a few weeks to get published and a few weeks for internal review.
- **Dedicated Project under Alliance Group**: If you’re looking for a home or an entrypoint for your joint solution on Gitlab.com, you can request a GitLab SubGroup within our Alliance group [here](https://gitlab.com/gitlab-com/alliances). Please submit an issue [here](https://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_sub-group_request) using the template *New Subgroup Request*. The partner Subgroup will be created as private until the prerequisites are filled out. See Issue template for more details. 
 
 
## Joint Opportunities
 
- **New Lead**: If you would like to team up with GitLab sales with a potential lead, [open a new issue](http://gitlab.com/gitlab-com/alliances/alliances/issues/new?issuable_template=new_lead) selecting the **New Lead** Issue Template, and a Partner Manager will route you to the correct Sales Account Owner at GitLab. We can schedule a follow up call in the issue comments if needed. 
> Note: **Please make sure the issue you create is confidential and private**. 

## Contact Us

We are here to help. The Alliance team works from issues and issue boards. If you are needing our assistance with any project, please [open an issue](http://gitlab.com/gitlab-com/alliances/alliances/issues/new) and we’ll get back to you as soon as we can! When creating an issue, please select *New_Partner* issue template in the drop down. If it’s technical assistance you’re looking for, please see below for troubleshooting.

## Troubleshooting
 
We're always here to help you through your efforts of integration. If there's a missing API call from our current API, or you ran into other difficulties in your development please feel free to create a new issue on the [Community Edition issue tracker](https://gitlab.com/gitlab-org/gitlab-foss/issues) and apply the `Ecosystem` label.


## GitLab.com Gold Subscription Sandbox Request

GitLab.com and GitLab EE share the same core code base. If you’re looking to quickly test and integrate with GitLab, often a project on GitLab.com can be the quickest way to get started. We’re happy to provision you a private sandbox subgroup in our [Alliances GitLab.com Group](https://Gitlab.com/gitlab-com/alliances) where you can create projects for demo, R&D, and testing purposes. To make the project/sandbox public to share with external parties outside of GitLab and Partner, we request you first complete the ReadMe.md file in your Public Project Repository. [Here](https://gitlab.com/gitlab-com/alliances/google/public-tracker) is an example. Also, it’s highly recommended to maintain a demo project as well for interested external parties.

## Requesting EE Dev License for R&D 

We are able to issue Ultimate licenses for integration testing and development upon request. These licenses are only open to those working on a GitLab Enterprise Edition specific integration. Licenses will be issued for 6 months and for up to 10 users. Please add it as a comment in the following [issue template](https://gitlab.com/gitlab-com/alliances/technology-partners/issues/new). 
